using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour
{
    private bool EstaTocandoElSuelo = false;
    private Rigidbody2D rb2d;
    private Transform transf;
    private SpriteRenderer sr;
    private bool derecha = false;
    private bool izquierda = false;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb2d = GetComponent<Rigidbody2D>();
        transf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if(izquierda == true)
        {
            sr.flipX = true;
            rb2d.velocity = new Vector2(-10, rb2d.velocity.y);
            //Debug.Log("camnina derecha");
        }
        else
        {
            sr.flipX = false;
            rb2d.velocity = new Vector2(10, rb2d.velocity.y);
            Debug.Log("camnina izquierda");
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        EstaTocandoElSuelo = true;


        if (other.gameObject.layer == 6)
        {
            
            izquierda = true;
            Debug.Log("toco derecha");
        }
        if (other.gameObject.layer == 7)
        {
            izquierda = false;
            Debug.Log("toco izquierda");
        }
    }
}

