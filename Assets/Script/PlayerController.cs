using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private bool EstaTocandoElSuelo = false;
    private Animator _animator;
    private Rigidbody2D rb2d;
    private Transform transf;
    private SpriteRenderer sr;
    public float upSpeed = 60;
    private bool muerto = false;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        transf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        float upSpeed = 60;

        if (muerto == false)
        {
            if (Input.GetKey(KeyCode.RightArrow))
            { //Derecha
                sr.flipX = false;
                setRunAnimation();
                rb2d.velocity = new Vector2(10, rb2d.velocity.y);//Intanciando vector
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    rb2d.velocity = new Vector2(30, rb2d.velocity.y);
                }
                if (Input.GetKeyDown(KeyCode.Space) && EstaTocandoElSuelo)
                {
                    setJumpAnimation();
                    rb2d.velocity = Vector2.up * upSpeed;
                    EstaTocandoElSuelo = false;
                }

            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            { //Izquierda
                sr.flipX = true;
                setRunAnimation();
                rb2d.velocity = new Vector2(-10, rb2d.velocity.y);
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    rb2d.velocity = new Vector2(-30, rb2d.velocity.y);
                }

                if (Input.GetKeyDown(KeyCode.Space) && EstaTocandoElSuelo)
                {
                    setJumpAnimation();
                    rb2d.velocity = Vector2.up * upSpeed;
                    EstaTocandoElSuelo = false;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Space) && EstaTocandoElSuelo)
            {
                setJumpAnimation();
                rb2d.velocity = Vector2.up * upSpeed;
                EstaTocandoElSuelo = false;


            }
            else
            {
                setIdleAnimation();
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            }
        }
        else
        {
            setDeadAnimation();
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        EstaTocandoElSuelo = true;
        if (other.gameObject.layer == 8)
        {

            muerto = true;
            Debug.Log("toco derecha");
        }
    }
    private void setIdleAnimation()
    {
        _animator.SetInteger("Estado", 0);
    }
    private void setRunAnimation()
    {
        _animator.SetInteger("Estado", 1);
    }
    private void setJumpAnimation()
    {
        _animator.SetInteger("Estado", 2);
    }
    private void setDeadAnimation()
    {
        _animator.SetInteger("Estado", 3);
    }



}
